*** Variables ***
${URL_LOGIN}                    https://3.0.60.173/login
${URL_KEY_IN_DATA_ENTRY}        https://3.0.60.173/short-data-entry/6207000000099
${INPUT_USERNAME}               id:usernameInput
${INPUT_PASSWORD}               id:passwordInput
${BUTTON_LOGIN}                 id:btn-login
${DATA_USERNAME}                30000283
${DATA_PASSWORD}                password
${BROWSER}                      Chrome
${NAVBAR_BRAND}                 xpath:/html/body/app-root/app-main/app-top-navbar/nav/a
${ROW_WORKLIST}                 xpath:/html/body/app-root/app-main/div/app-to-do-list/div[2]/div/app-dt-datatable/div/table/tbody/tr/td[10]

# database
${DB_HOST}                          sit-loan-origination.ckt9r9vhy9g5.ap-southeast-1.rds.amazonaws.com
${DB_NAME}                          cow-replicate
${DB_SCHEMA_COW_APPLICATION}        use `cow-application`;
${DB_SCHEMA_COW_REPLICATE}          use `cow-replicate`;
${DB_USERNAME}                      root_aws
${DB_PASSWORD}                      xJxwKNU3
${DB_PORT}                          3306        

${DETAIL_HEADER_INPUT_PRODUCT_CODE}             id:productCode
${DETAIL_HEADER_DISABLED_PRODUCT_NAME}          id:productName
${DETAIL_HEADER_INPUT_BRANCH_CODE}              id:branchCode
${DETAIL_HEADER_DISABLED_BRANCH_NAME}           id:branchName
${DETAIL_SHORTINFO_INPUT_CAMPAIGN_CODE}         css=#shortKeyin > app-keyin-short-information > form > div:nth-child(2) > div.col-12.form-group > input
${DETAIL_SHORTINFO_INPUT_CAMPAIGN_CODE}         id:campaignCode
${DETAIL_SHORTINFO_DISABLED_CAMPAIGN_NAME}      id:campaignName
${DETAIL_SHORTINFO_INPUT_DEALER_CODE}           id:dealerCode
${DETAIL_SHORTINFO_INPUT_MARKETING_CODE}        id:marketingCode
${DETAIL_SHORTINFO_INPUT_TELESALE_CODE}         id:telesaleCode
${DETAIL_BODYMAKER}                             id:bodyMaker
${DETAIL_BUTTON_NEXT}                           id:btn-next
${DETAIL_BUTTON_BACK}                           id:btn-back
${DETAIL_BUTTON_SAVEDRAFT}                      id:saveDraftButton
${DETAIL_BUTTON_CREATE_REQUEST}                 id:createRequestButton
${DETAIL_MARKETING_DATE}                        id:marketingDate
${DETAIL_PPI_INPUT_MARKETING_TIME}              id:marketingTime
${DETAIL_PPI_INPUT_PPI_APP_DATE}                id:ppiAppDate
${DETAIL_PPI_INPUT_MR_CODE_PPI}                 id:ppiMrCode
${DETAIL_CUSTOMER_INPUT_RADIO_INDIVIDUAL}       css=mat-radio-group#customerType > mat-radio-button:nth-child(1)
${DETAIL_CUSTOMER_INPUT_RADIO_INDIVIDUAL}       css=mat-radio-group#customerType > mat-radio-button:nth-child(2)
${DETAIL_CUSTOMER_INPUT_CARD_TYPE}              id:cardTypeCode
${DETAIL_CUSTOMER_INPUT_TITLE_TH}               id:titleTh
${DETAIL_CUSTOMER_INPUT_NAME_TH}                id:nameTh
${DETAIL_CUSTOMER_INPUT_SURNAME_TH}             id:surnameCompanyTh
${DETAIL_CUSTOMER_INPUT_TITLE_EN}               id:titleEn
${DETAIL_CUSTOMER_INPUT_NAME_EN}                id:nameEn
${DETAIL_CUSTOMER_INPUT_SURNAME_EN}             id:surnameCompanyEn
${DETAIL_CUSTOMER_INPUT_IDCARD}                 id:identityCard  
${DETAIL_CUSTOMER_RADIO_VAT_FLAG_YES}           css:mat-radio-group#vatFlag > mat-radio-button:nth-child(1)
${DETAIL_CUSTOMER_RADIO_VAT_FLAG_NO}            css:mat-radio-group#vatFlag > mat-radio-button:nth-child(2)
${DETAIL_CUSTOMER_INPUT_VAT_BRANCH_NO}          id:vatBranchNo
${DETAIL_CUSTOMER_INPUT_VAT_BRANCH_NAME}        id:vatBranchName
${DETIAL_EXISTING_DATA_TABLE}                   css:#existingCustomerTab > table > tbody
# ${POPUP_MESSAGE_ERROR}                          css:#success-toast > div > p-toastitem > div > div > a

${DETAIL_APP_INPUT_FAX_IN_CA_DATE}              id:faxInCaDate
${DETAIL_APP_INPUT_FAX_IN_CA_TIME}              id:faxInCaTime
${DETAIL_APP_INPUT_APPLICATION_DATE}            id:applicationDate
${DETAIL_APP_RADIO_SCORING_CONTEST_1}           css:#scoringConsent > mat-radio-button:first-child
# ${DETAIL_APP_RADIO_SCORING_CONTEST_2}           css:#scoringConsentInput > mat-radio-button:nth-child(2)
${DETAIL_APP_INPUT_NCB15}                       id:ncb15Code
${DETAIL_APP_INPUT_BIRTH_DATE}                  id:birthDate
${DETAIL_APP_INPUT_NATIONALITY}                 id:nationalityCode
${DETAIL_APP_INPUT_OFFICE}                      id:officeCode

${DETAIL_GUARANTOR_ICON_PLUS}                   xpath://*[@id="newGuarantorButton"]/fa-icon/svg
${DETAIL_GUARANTOR_BUTTON_NEWGUARANTOR}         id:newGuarantorButton
${DETAIL_GUARANTOR_RADIO_TYPE}                  css:mat-radio-group#guarantorTypeInput > mat-radio-button:nth-child(1)
${DETAIL_GUARANTOR_INPUT_CARD_TYPE}             id:cardTypeCode
${DETAIL_GUARANTOR_INPUT_TITLE_TH}              id:guarantorTitleThInput
${DETAIL_GUARANTOR_INPUT_TH_NAME}               id:guarantorNameThInput
${DETAIL_GUARANTOR_INPUT_TH_SURNAME}            id:guarantorSurnameThInput
${DETAIL_GUARANTOR_INPUT_EN_NAME}               id:guarantorNameEnInput
${DETAIL_GUARANTOR_INPUT_EN_SURNAME}            id:guarantorSurnameEnInput
${DETAIL_GUARANTOR_INPUT_ID_CARD}               id:guarantorIdentityCardInput
${DETAIL_GUARANTOR_INPUT_BIRTH_DATE}            id:guarantorBirthDateInput
${DETAIL_GUARANTOR_INPUT_NATIONALITY}           id:guarantorNationalityCodeInput
${DETAIL_GUARANTOR_INPUT_OFFICE}                id:guarantorOfficeCodeInput
${DETAIL_GUARANTOR_INPUT_CONTACT}               id:guarantorBillingContactInput
${DETAIL_GUARANTOR_INPUT_TELEPHONE}             id:guarantorBillingContactInput
${DETAIL_GUARANTOR_INPUT_ADDRESS}               id:guarantorBillingAddressInput
${DETAIL_GUARANTOR_INPUT_ZIPCODE}               id:guarantorZipcodeInput
${DETAIL_GUARANTOR_INPUT_PROVINCE}              id:guarantorProvinceInput
${DETAIL_GUARANTOR_INPUT_DISTRICT}              id:guarantorDistrictInput
${DETAIL_GUARANTOR_INPUT_SUB_DISTRICT}          id:guarantorSubDistrictInput
${DETAIL_GUARANTOR_BUTTON_CANCEL}               id:cancelGuarantorButton
${DETAIL_GUARANTOR_BUTTON_OK}                   id:saveGuarantorButton

# DATA CUSTOMER - IKEY
${DATA_HEADER_PRODUCT_CODE}                     HP
${DATA_HEADER_BRANCH_CODE}                      11
${DATA_SHORTINFO_CAMPAIGN_CODE}                 201A4CA
${DATA_SHORTINFO_DEALER_CODE}                   01BAA
${DATA_SHORTINFO_MARKETING_CODE}                106
${DATA_SHORTINFO_TELESALE_CODE}                 
${DATA_MARKETING_DATE}                          26/07/62
${DATA_PPI_MARKETING_DATE}                      26/07/62
${DATA_PPI_MARKETING_TIME}                      10:00
${DATA_PPI_APP_DATE}                            26/07/62
${DATA_PPI_MR_CODE_PPI}                         106
${DATA_APP_INPUT_APPLICATION_DATE}              26/07/62
${DATA_CUSTOMER_CARD_TYPE}                      1
${DATA_CUSTOMER_TITLE_TH}                       นาย
${DATA_CUSTOMER_NAME_TH}                        เอ็ดวิน
${DATA_CUSTOMER_SURNAME_TH}                     ฟานเดอร์ซาร์
${DATA_CUSTOMER_NAME_EN}                        Edwin
${DATA_CUSTOMER_SURNAME_EN}                     Van der sar
${DATA_CUSTOMER_IDCARD}                         1234567891011
${DATA_CUSTOMER_VAT_BRANCH_NO}                  11
${DATA_CUSTOMER_VAT_BRANCH_NAME}                สำนักงานใหญ่
${DATA_APP_FAX_IN_CA_DATE}                      26/07/62
${DATA_APP_FAX_IN_CA_TIME}                      11:00
${DATA_APP_NCB}                                 Y
${DATA_APP_BIRTH_DATE}                          12/12/2499
${DATA_APP_NATIONALITY}                         THI
${DATA_APP_OFFICE}                              001

# DATA GUARANTOR - IKEY
${DATA_GUARANTOR_INPUT_CARD_TYPE}               1
${DATA_GUARANTOR_INPUT_TITLE_TH}                พล.ต.อ.
${DATA_GUARANTOR_INPUT_TH_NAME}                 เดวิด
${DATA_GUARANTOR_INPUT_TH_SURNAME}              มอย
${DATA_GUARANTOR_INPUT_EN_NAME}                 David
${DATA_GUARANTOR_INPUT_EN_SURNAME}              Beckham
${DATA_GUARANTOR_INPUT_ID_CARD}                 3659162081879
${DATA_GUARANTOR_INPUT_BIRTH_DATE}              04042499
${DATA_GUARANTOR_INPUT_NATIONALITY}             THI
${DATA_GUARANTOR_INPUT_OFFICE}                  008
${DATA_GUARANTOR_INPUT_CONTACT}                 0988888888
${DATA_GUARANTOR_INPUT_TELEPHONE}               028975963
${DATA_GUARANTOR_INPUT_ADDRESS}                 555/5555
${DATA_GUARANTOR_INPUT_ZIPCODE}                 10220
${DATA_GUARANTOR_INPUT_PROVINCE}                กรุงเทพมหานคร
${DATA_GUARANTOR_INPUT_DISTRICT}                บางเขน
${DATA_GUARANTOR_INPUT_SUB_DISTRICT}            อนุสาวรีย์

${BUTTON_CREATE_REQUEST}                        xpath:/html/body/app-root/app-main/div/app-short-data-entry/div/div[2]/button

# ERROR MESSAGE
${ERROR_PRODUCT_CODE}                           id:productCodeError
${ERROR_BRANCH_CODE}                            id:branchCodeError
${ERROR_CAMPAIGN_CODE}                          id:campaignCodeError