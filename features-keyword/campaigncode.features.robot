*** Keywords ***
Set Screen Directory
    [Arguments]     ${CASE_NO}
    Set Screenshot Directory    /Users/knot/Documents/workspaces/cow-data-entry/screenshot/campaigncode-screenshot/${CASE_NO}/
    # Capture Page Screenshot     step-{index}.png
Connect database
    Connect To Database         pymysql    ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}
    ${USE_COW_REPLICATE}        Execute SQL String      ${DB_SCHEMA_COW_REPLICATE}
Campaign code match in database
    ${SQL_RESULT}               Query           select gccode from hp1p36 Where gccomp = 'GECAL' and gcprod = 'HP' and GCSTS in ('A') limit 1;
    # Log to Console      \n${SQL_RESULT}
    # ${CAMPAIGNCODE_VALUE}       Set Variable    ${SQL_RESULT[0][0]}
    Input Text                  ${DETAIL_SHORTINFO_INPUT_CAMPAIGN_CODE}     ${SQL_RESULT[0][0]}
Press key Tab
    Press Key       ${DETAIL_HEADER_INPUT_PRODUCT_CODE}     \\09
Verify campaigncode
    Element Should Be Disabled      ${DETAIL_SHORTINFO_DISABLED_CAMPAIGN_NAME}
    Wait Until Element Is Visible   ${DETAIL_SHORTINFO_DISABLED_CAMPAIGN_NAME}
    Page Should Not Contain Element     ${ERROR_CAMPAIGN_CODE}
Click button Create request
    Click Button                ${BUTTON_CREATE_REQUEST}
Verify campaigncode error
    [Arguments]             ${ARG_CAMPAIGN_CODE_ERROR}
    Wait Until Element Is Visible   ${ERROR_CAMPAIGN_CODE}
    Element Text Should Be          ${ERROR_CAMPAIGN_CODE}      ${ARG_CAMPAIGN_CODE_ERROR}
Input campaign code status in ('I', 'A')
    ${SQL_RESULT}               Query           select gccode from hp1p36 Where gccomp = 'GECAL' and gcprod = 'HP' and GCSTS in ('A') limit 1;
    Input Text                  ${DETAIL_SHORTINFO_INPUT_CAMPAIGN_CODE}     ${SQL_RESULT[0][0]}
Input campaign code status not in ('I', 'A')
    ${SQL_RESULT}               Query           select gccode from hp1p36 Where gccomp = 'GECAL' and gcprod = 'HP' and GCSTS not in ('I', 'A') limit 1;
    Input Text                  ${DETAIL_SHORTINFO_INPUT_CAMPAIGN_CODE}     ${SQL_RESULT[0][0]}
Input campiagncode is correct and Campaign CRM Product = HP
    ${SQL_RESULT}               Query           Select gccode from hp1p36 Where gccomp = 'GECAL' and gcprod = 'HP' and GCSTS in ('A') and gccode like '__C%' limit 1;
    Input Text                  ${DETAIL_SHORTINFO_INPUT_CAMPAIGN_CODE}     ${SQL_RESULT[0][0]}
Input campiagncode is correct and Campaign CRM Product = MC
    ${SQL_RESULT}               Query           Select gccode from hp1p36 Where gccomp = 'GECAL' and gcprod = 'MC' and GCSTS in ('A') and gccode like '_____1%' LIMIT 1;
    Input Text                  ${DETAIL_SHORTINFO_INPUT_CAMPAIGN_CODE}     ${SQL_RESULT[0][0]}
Input campiagncode is correct and campaign is truck only
    ${SQL_RESULT}               Query           Select gccode, gcname from hp1p36 h Where h.gccode like 'T%' and h.GCSTS in ('A') LIMIT 1;
    Input Text                  ${DETAIL_SHORTINFO_INPUT_CAMPAIGN_CODE}     ${SQL_RESULT[0][0]}
Input campiagncode is correct and campaign is not truck
    ${SQL_RESULT}               Query           SELECT gccode from hp1p36 h Where h.gccode like 'G%' and h.GCSTS NOT IN ('A', 'I') LIMIT 1;
    Input Text                  ${DETAIL_SHORTINFO_INPUT_CAMPAIGN_CODE}     ${SQL_RESULT[0][0]}
