*** Keywords ***
Set Screen Directory
    [Arguments]     ${CASE_NO}
    Set Screenshot Directory    /Users/knot/Documents/workspaces/cow-data-entry/screenshot/branchcode-screenshot/${CASE_NO}/
Input Branch code
    [Arguments]     ${ARG_BRANCH_CODE}
    Input Text      ${DETAIL_HEADER_INPUT_BRANCH_CODE}     ${ARG_BRANCH_CODE}
    Capture Page Screenshot     step-{index}.png
Press key Tab
    Press Key       ${DETAIL_HEADER_INPUT_BRANCH_CODE}     \\09
Verify Branch
    [Arguments]     ${ARG_BRANCH_NAME}
    Element Should Be Disabled      ${DETAIL_HEADER_DISABLED_BRANCH_NAME}
    Textfield Value Should Be          ${DETAIL_HEADER_DISABLED_BRANCH_NAME}      ${ARG_BRANCH_NAME}
    Capture Page Screenshot     step-{index}.png
Click button Create request
    Click Button                ${BUTTON_CREATE_REQUEST}
Verify Branch code error
    [Arguments]             ${ARG_BRANCH_CODE_ERROR}
    Element Text Should Be          ${ERROR_BRANCH_CODE}      ${ARG_BRANCH_CODE_ERROR}
    Capture Page Screenshot     step-{index}.png