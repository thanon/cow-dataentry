*** Keywords ***
Login Loan Origination (Credit on Web)
    [Arguments]     ${VALUE_USERNAME}   ${VALUE_PASSWORD}
    Open Browser    ${URL_LOGIN}      ${BROWSER}
    Maximize Browser Window
    Input Text      ${INPUT_USERNAME}   ${VALUE_USERNAME}
    Input Text      ${INPUT_PASSWORD}   ${VALUE_PASSWORD}
    Click Button    ${BUTTON_LOGIN}
    # Wait Until Page Contains Element    ${NAVBAR_BRAND}     timeout=3
    # Go to   ${ROW_WORKLIST}
    Wait Until Element Is Visible       ${ROW_WORKLIST}
    Click Element           ${ROW_WORKLIST}
    # Wait Until Element Contains  ${HEADER_MODULE}  IKEY(Data Entry)  timeout=3
    # Element Text Should Be       ${HEADER_MODULE}  IKEY(Data Entry)  