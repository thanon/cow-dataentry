*** Keywords ***
# Login Loan Origination (Credit on Web)
#     [Arguments]     ${VALUE_USERNAME}   ${VALUE_PASSWORD}
#     Open Browser    ${URL_LOGIN}      ${BROWSER}
#     Maximize Browser Window
#     Input Text      ${INPUT_USERNAME}   ${VALUE_USERNAME}
#     Input Text      ${INPUT_PASSWORD}   ${VALUE_PASSWORD}
#     Click Button    ${BUTTON_LOGIN}
#     Wait Until Page Contains Element    ${NAVBAR_BRAND}     timeout=3
#     Go to   ${URL_KEY_IN_DATA_ENTRY}
    # Wait Until Element Contains  ${HEADER_MODULE}  IKEY(Data Entry)  timeout=3
    # Element Text Should Be       ${HEADER_MODULE}  IKEY(Data Entry)  
Key data ProductCode
    Input Text              ${DETAIL_HEADER_INPUT_PRODUCT_CODE}     ${DATA_HEADER_PRODUCT_CODE}
key data BranchCode
    Input Text              ${DETAIL_HEADER_INPUT_BRANCH_CODE}      ${DATA_HEADER_BRANCH_CODE}
key data CampaignCode
    Input Text              ${DETAIL_SHORTINFO_INPUT_CAMPAIGN_CODE}     ${DATA_SHORTINFO_CAMPAIGN_CODE}
key data DealerCode
    Input Text              ${DETAIL_SHORTINFO_INPUT_DEALER_CODE}       ${DATA_SHORTINFO_DEALER_CODE}
key data MarketingCode
    Input Text              ${DETAIL_SHORTINFO_INPUT_MARKETING_CODE}    ${DATA_SHORTINFO_MARKETING_CODE}
key data TelesaleCode
    Input Text              ${DETAIL_SHORTINFO_INPUT_TELESALE_CODE}     ${DATA_SHORTINFO_TELESALE_CODE}
Key data Marketing Date
    Input Text              ${DETAIL_MARKETING_DATE}        ${DATA_PPI_MARKETING_DATE}
Click button Next
    Wait Until Element Is Visible       ${DETAIL_BUTTON_NEXT}
    Click Button            ${DETAIL_BUTTON_NEXT}
Click button Next2
    Wait Until Element Is Not Visible       css:#success-toast > div    timeout=30
    Click Button            ${DETAIL_BUTTON_NEXT}
key data MarketingTime
    Input Text              ${DETAIL_PPI_INPUT_MARKETING_TIME}          ${DATA_PPI_MARKETING_TIME}
key data PPI App Date
    Input Text              ${DETAIL_PPI_INPUT_PPI_APP_DATE}            ${DATA_PPI_APP_DATE}
key data MR Code PPI
    Input Text              ${DETAIL_PPI_INPUT_MR_CODE_PPI}             ${DATA_PPI_MR_CODE_PPI}
Select Customer Type
    # Wait Until Element Is Visible   ${DETAIL_MAT_RADIO_GROUP_CUSTOMER_TYPE_INPUT}
    Click Element           ${DETAIL_CUSTOMER_INPUT_RADIO_INDIVIDUAL}
key data Card Type
    Input Text              ${DETAIL_CUSTOMER_INPUT_CARD_TYPE}          ${DATA_CUSTOMER_CARD_TYPE}
key data TitleTH
    Input Text              ${DETAIL_CUSTOMER_INPUT_TITLE_TH}           ${DATA_CUSTOMER_TITLE_TH}
key data NameTH
    Input Text              ${DETAIL_CUSTOMER_INPUT_NAME_TH}            ${DATA_CUSTOMER_NAME_TH}
key data SurnameTH
    Input Text              ${DETAIL_CUSTOMER_INPUT_SURNAME_TH}         ${DATA_CUSTOMER_SURNAME_TH}
key data NameEN
    Input Text              ${DETAIL_CUSTOMER_INPUT_NAME_EN}            ${DATA_CUSTOMER_NAME_EN}
key data SurnameEN
    Input Text              ${DETAIL_CUSTOMER_INPUT_SURNAME_EN}         ${DATA_CUSTOMER_SURNAME_EN}
key data ID Card
    Input Text              ${DETAIL_CUSTOMER_INPUT_IDCARD}             ${DATA_CUSTOMER_IDCARD}
    Log to Console          \nkey ID CARD
Select Vat Flag
    Wait Until Element Is Not Visible       css:#success-toast > div    timeout=60
    Click Element           ${DETAIL_CUSTOMER_RADIO_VAT_FLAG_YES}
    Log to Console          \nSelect Vat Flag
key data Vat Branch No
    Wait Until Element Is Not Visible       css:#success-toast > div
    Input Text              ${DETAIL_CUSTOMER_INPUT_VAT_BRANCH_NO}      ${DATA_CUSTOMER_VAT_BRANCH_NO}
key data Vat Branch Name    
    Input Text              ${DETAIL_CUSTOMER_INPUT_VAT_BRANCH_Name}    ${DATA_CUSTOMER_VAT_BRANCH_NAME}
Show data table Existing
    ${VALUE}    Get Text    ${DETIAL_EXISTING_DATA_TABLE}
    Run Keyword IF  '${VALUE}' == '${EMPTY}'        Run Keywords    Close popup error
    ...     AND     Log to Console  \nClick Next
    ...     ELSE IF     '${VALUE}' != '${EMPTY}'    Run Keywords    Close popup error
    ...     AND     Log to Console  \nTable #existingCustomerTab > table is value is not empty.
Key data Fax In CA short form Date
    Input Text              ${DETAIL_APP_INPUT_FAX_IN_CA_DATE}       ${DATA_APP_FAX_IN_CA_DATE}
Key data Fax In CA short form Time
    Input Text              ${DETAIL_APP_INPUT_FAX_IN_CA_TIME}       ${DATA_APP_FAX_IN_CA_TIME}
Key data Application Date
    Input Text              ${DETAIL_APP_INPUT_APPLICATION_DATE}       ${DATA_APP_INPUT_APPLICATION_DATE}
Select Scoring Consent NCB15
    Wait Until Element Is Visible       ${DETAIL_APP_RADIO_SCORING_CONTEST_1} 
    Click Element           ${DETAIL_APP_RADIO_SCORING_CONTEST_1}
Key data NCB15
    Input Text              ${DETAIL_APP_INPUT_NCB15}           ${DATA_APP_NCB}
key data Birth Date
    Input Text              ${DETAIL_APP_INPUT_BIRTH_DATE}      ${DATA_APP_BIRTH_DATE}
key data Nationality
    Input Text              ${DETAIL_APP_INPUT_NATIONALITY}     ${DATA_APP_NATIONALITY}
key data Office
    Input Text              ${DETAIL_APP_INPUT_OFFICE}          ${DATA_APP_OFFICE}
Close popup error
    Wait Until Element Is Visible       css:#success-toast > div
    Click Link           css:#success-toast > div > p-toastitem > div > div > a
Click button New Guarantor
    Click Button            ${DETAIL_GUARANTOR_BUTTON_NEWGUARANTOR}
Click Guarantor Type by Guarantor
    Click Element           ${DETAIL_GUARANTOR_RADIO_TYPE}
Key data Card Type by Guarantor
    Input Text              ${DETAIL_CUSTOMER_INPUT_CARD_TYPE}         ${DATA_GUARANTOR_INPUT_CARD_TYPE}
key data TitleTH by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_TITLE_TH}          ${DATA_GUARANTOR_INPUT_TITLE_TH}
key data NameTH by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_TH_NAME}           ${DATA_GUARANTOR_INPUT_TH_NAME}
key data SurnameTH by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_TH_SURNAME}        ${DATA_GUARANTOR_INPUT_TH_SURNAME}
key data NameEN by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_EN_NAME}           ${DATA_GUARANTOR_INPUT_EN_NAME}
key data SurnameEN by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_EN_SURNAME}        ${DATA_GUARANTOR_INPUT_EN_SURNAME}
key data ID Card by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_ID_CARD}           ${DATA_GUARANTOR_INPUT_ID_CARD}
key data Birth Date by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_BIRTH_DATE}        ${DATA_GUARANTOR_INPUT_BIRTH_DATE}
key data Nationality by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_NATIONALITY}       ${DATA_GUARANTOR_INPUT_NATIONALITY}
key data Office by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_OFFICE}            ${DATA_GUARANTOR_INPUT_OFFICE}
key data Contact by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_CONTACT}           ${DATA_GUARANTOR_INPUT_CONTACT}
key data Telephone by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_TELEPHONE}         ${DATA_GUARANTOR_INPUT_TELEPHONE}
key data Address by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_ADDRESS}           ${DATA_GUARANTOR_INPUT_ADDRESS}
key data Zipcode by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_ZIPCODE}           ${DATA_GUARANTOR_INPUT_ZIPCODE}
key data Province by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_PROVINCE}          ${EMPTY}
    Input Text              ${DETAIL_GUARANTOR_INPUT_PROVINCE}          ${DATA_GUARANTOR_INPUT_PROVINCE}
key data District by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_DISTRICT}          ${EMPTY}
    Input Text              ${DETAIL_GUARANTOR_INPUT_DISTRICT}          ${DATA_GUARANTOR_INPUT_DISTRICT}
key data Sub District by Guarantor
    Input Text              ${DETAIL_GUARANTOR_INPUT_SUB_DISTRICT}      ${EMPTY}
    Input Text              ${DETAIL_GUARANTOR_INPUT_SUB_DISTRICT}      ${DATA_GUARANTOR_INPUT_SUB_DISTRICT}
Click Button Cancel by Guarantor
    Click Button            ${DETAIL_GUARANTOR_BUTTON_CANCEL}
Click Button OK by Guarantor
    Click Button            ${DETAIL_GUARANTOR_BUTTON_OK}
Click Button Save Draft
    Click Button            ${DETAIL_BUTTON_SAVEDRAFT}
    Log to Console          \nSave Draft
Click Button Create request
    Click Button            ${DETAIL_BUTTON_CREATE_REQUEST}