*** Setting ***
# Suite Setup       Connect To Database    pymysql    ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}
Library         Selenium2Library
Library         DatabaseLibrary
# Library         String
# Library         Collections
Suite Teardown  Disconnect From Database


*** Variables ***
${DB_HOST}                          sit-loan-origination.ckt9r9vhy9g5.ap-southeast-1.rds.amazonaws.com
${DB_NAME}                          cow-replicate
${DB_SCHEMA_COW_APPLICATION}        use `cow-application`;
${DB_SCHEMA_COW_REPLICATE}          use `cow-replicate`;
${DB_USERNAME}                      root_aws
${DB_PASSWORD}                      xJxwKNU3
${DB_PORT}                          3306         

*** Test Cases ***
Connect database
    GIVEN Connect database
    AND Select Campaign Code

*** Keywords ***
Connect database
    Connect To Database    pymysql    ${DB_NAME}    ${DB_USERNAME}    ${DB_PASSWORD}    ${DB_HOST}    ${DB_PORT}
    ${USE_COW_REPLICATE}          Execute SQL String      ${DB_SCHEMA_COW_REPLICATE}
Select Campaign Code
    ${SQL_RESULT}       Query      Select gccode from hp1p36 Where gccomp = 'GECAL' and gcprod = 'HP' and GCSTS in ('A') limit 1;
    Log to Console          ${SQL_RESULT[0][0]}
    # Log to Console          \n@{SQL_RESULT}
    # Log to Console          \n${SQL_RESULT[0][0]}
    # ${CAMPAIGNCODE_VALUE}     Set Variable   ${SQL_RESULT[0][0]}
    # Log to Console          ${CAMPAIGNCODE_VALUE}
    # Log to Console      \n${SQL_RESULT[0][0]}

    # Run keyword if      '${SQL_RESULT[0][0]}' == '201A4CA'   Log to Console  hello