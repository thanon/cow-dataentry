*** Keywords ***
Set Screen Directory
    [Arguments]     ${CASE_NO}
    Set Screenshot Directory    /Users/knot/Documents/workspaces/cow-data-entry/screenshot/product-screenshot/${CASE_NO}/
Input Product code
    [Arguments]     ${ARG_PRODUCT_CODE}
    Input Text      ${DETAIL_HEADER_INPUT_PRODUCT_CODE}     ${ARG_PRODUCT_CODE}
    Capture Page Screenshot     step-{index}.png
Press key Tab
    Press Key       ${DETAIL_HEADER_INPUT_PRODUCT_CODE}     \\09
Verify Product
    [Arguments]     ${ARG_PRODUCT_NAME}
    Element Should Be Disabled      ${DETAIL_HEADER_DISABLED_PRODUCT_NAME}
    Textfield Value Should Be          ${DETAIL_HEADER_DISABLED_PRODUCT_NAME}      ${ARG_PRODUCT_NAME}
    Capture Page Screenshot     step-{index}.png
    # ${RESULT_PRODUCT_NAME}      Get Value        ${DETAIL_HEADER_DISABLED_PRODUCT_NAME}
    # Log to Console      ${RESULT_PRODUCT_NAME}
Click button Create request
    Click Button                ${BUTTON_CREATE_REQUEST}
Verify Product code error
    [Arguments]             ${ARG_PRODUCT_CODE_ERROR}
    Element Text Should Be          ${ERROR_PRODUCT_CODE}      ${ARG_PRODUCT_CODE_ERROR}
    Capture Page Screenshot     step-{index}.png