*** Settings ***
Library         Selenium2Library
# Resource        ../features-keyword/create-request-data-entry.features.robot
Resource        ../features-keyword/branchcode.features.robot
Resource        ../features-keyword/login.features.robot
Resource        ../variables-testdata/data-test.robot
Test Setup      Run keyword     Login Loan Origination (Credit on Web)     ${DATA_USERNAME}    ${DATA_PASSWORD}
Test Teardown   Close Browser

*** Test Cases ***
กรณีที่กรอกข้อมูล Branch ถูกต้อง
    [Tags]  Case1
    GIVEN Set Screen Directory              Case1
    AND Input Branch code                   11
    WHEN Press key Tab
    THEN Verify Branch                      อาคารแคปปิตอล ชั้น 3
กรณีที่กรอกข้อมูล Branch เป็นค่าว่าง
    [Tags]  Case2
    GIVEN Set Screen Directory              Case2
    AND Click button Create request
    THEN Verify Branch code error           Branch code must not be empty.
กรณีที่กรอก Branch ที่ไม่มีอยู่ใน Database
    [Tags]  Case3
    GIVEN Set Screen Directory              Case3 
    AND Input Branch code                  ABCDEF
    WHEN Press key Tab
    THEN Verify Branch code error          This branch doesn't exist in database