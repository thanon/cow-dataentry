*** Settings ***
Library         Selenium2Library
Library         DatabaseLibrary
Resource        ../features-keyword/campaigncode.features.robot
Resource        ../features-keyword/login.features.robot
Resource        ../variables-testdata/data-test.robot
Suite Setup     Run keyword     Connect database
Test Setup      Run keyword     Login Loan Origination (Credit on Web)     ${DATA_USERNAME}    ${DATA_PASSWORD}
Test Teardown   Close Browser
Suite Teardown  Disconnect From Database

*** Test Cases ***
# Connect database
#     GIVEN Connect database
กรณีที่มีข้อมูล Campaign Code ในฐานข้อมูล
    [Tags]  Case1
    GIVEN Campaign code match in database
    WHEN Press key Tab
    THEN Verify campaigncode
กรณีที่ส่งค่า Campaign Code เป็นค่าว่าง
    [Tags]  Case2
    GIVEN Click button Create request
    THEN Verify campaigncode error      Campaign code must not be empty.
กรณีที่ Campaign Code มี status IN ('I', 'A')
    [Tags]  Case3
    GIVEN Input campaign code status in ('I', 'A')
    WHEN Press key Tab
    THEN Verify campaigncode
กรณีที่ Campaign Code มี status NOT IN ('I', 'A')
    [Tags]  Case4
    GIVEN Input campaign code status not in ('I', 'A')
    WHEN Press key Tab
    THEN Verify campaigncode error      ไม่พบ Campaign Code / Campaign code is invalid
กรณีที่ Campaign Code ถูกต้องและเป็น A@B Campaign CRM Product = HP Campaign digit 3rd = 'C'
    [Tags]  Case5
    GIVEN Input campiagncode is correct and Campaign CRM Product = HP
    WHEN Press key Tab
    THEN Verify campaigncode
กรณีที่ Campaign Code ถูกต้องและเป็น A@B Campaign CRM Product = MC
    [Tags]  Case6
    GIVEN Input campiagncode is correct and Campaign CRM Product = MC
    WHEN Press key Tab
    THEN Verify campaigncode
กรณีที่ ค่า campaign code ถูกต้องEntity เป็น A@B และ Campaign เป็น รถบรรทุก(Campaign code หลักแรกเป็น 'G' หรือ 'S' หรือ 'T')
    [Tags]  Case7
    GIVEN Input campiagncode is correct and campaign is truck only
    WHEN Press key Tab
    THEN Verify campaigncode
กรณีที่ ค่า campaign code ถูกต้องEntity ไม่เป็น A@B และ Campaign ไม่เป็น รถบรรทุก(Campaign code หลักแรกเป็น 'G' หรือ 'S' หรือ 'T')
    [Tags]  Case8
    GIVEN Input campiagncode is correct and campaign is not truck
    WHEN Press key Tab
    THEN Verify campaigncode error      ไม่พบ Campaign Code / Campaign code is invalid