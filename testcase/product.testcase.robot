*** Settings ***
Library         Selenium2Library
# Resource        ../features-keyword/create-request-data-entry.features.robot
Resource        ../features-keyword/product.features.robot
Resource        ../features-keyword/login.features.robot
Resource        ../variables-testdata/data-test.robot
Test Setup      Run keyword     Login Loan Origination (Credit on Web)     ${DATA_USERNAME}    ${DATA_PASSWORD}
Test Teardown   Close Browser

*** Test Cases ***
กรณีที่กรอกข้อมูล Product ถูกต้อง
    [Tags]  Case1
    GIVEN Set Screen Directory              Case1
    AND Input Product code                  HP
    WHEN Press key Tab
    THEN Verify Product                     Hire Purchase
กรณีที่กรอกข้อมูล Product เป็นค่าว่าง
    [Tags]  Case2
    GIVEN Set Screen Directory              Case2
    AND Click button Create request
    THEN Verify Product code error          Product code must not be empty.
กรณีที่กรอกข้ Product ไม่มีอยู่ใน Database
    [Tags]  Case3
    GIVEN Set Screen Directory              Case3 
    AND Input Product code                  ABCDEF
    WHEN Press key Tab
    THEN Verify Product code error          This product doesn't exist in database